<?php


    /* 
    * 
    *
    */

    $matriz = [[2,3],[3,5],[8,9]];
    
    
    function operacionBasica(array $matrizA, array $matrizB, $operacion = "+") {
        
        $operaciones = array("+","-","*");
        
        if(!in_array($operacion, $operaciones))
            return [];

        $resultado = matricesIgualTam($matrizA, $matrizB);

        if(count(matricesIgualTam($matrizA, $matrizB)) == 0)
            return [];
        
        $fila = $resultado[0];
        $columnas = $resultado[1];

        $matrizResultado = [];
    
        for ($i = 0; $i < $fila; $i++) {
            $filaResultado = [];
            for ($j = 0; $j < $columnas; $j++) {

                if($operacion == "+")
                    $elementoResultado = $matrizA[$i][$j] + $matrizB[$i][$j];
                if($operacion == "-")
                    $elementoResultado = $matrizA[$i][$j] - $matrizB[$i][$j];
                if($operacion == "*")
                    $elementoResultado = $matrizA[$i][$j] * $matrizB[$i][$j];
                        
                $filaResultado[] = $elementoResultado;
            }
            $matrizResultado[] = $filaResultado;
        }
    
        return $matrizResultado;
    }

    function escalar(array $matriz, int $value): array {
        $matrizEscalar = [];
        
        foreach ($matriz as $fila) {
            
            $filaEscalar = [];

            foreach ($fila as $elemento) {
                $elementoEscalar = $elemento * $value;
                $filaEscalar[] = $elementoEscalar;
            }

            $matrizEscalar[] = $filaEscalar;
        }

        return $matrizEscalar;
    }   

    function matricesIgualTam(array $matrizA, array $matrizB): array {
        $filasA = count($matrizA);
        $columnasA = count($matrizA[0]);
        $filasB = count($matrizB);
        $columnasB = count($matrizB[0]);
    
        if ($filasA !== $filasB || $columnasA !== $columnasB) {
            return [];
        }
        return [$filasA, $columnasA];
    }

    function echoMatriz($matriz) {
        echo "\n *******  Matriz calculada *******\n";
        foreach ($matriz as $fila) {
            echo "\n" . implode(" ", $fila) . "\n";
        }
        echo "\n *********************************\n";
    }


    while (true) {
        // Mostrar el menú
        echo "\n******** Bienvenido a la calculadora de Matrices ********\n";
        echo "Menú:\n";
        echo "1. Suma 1\n";
        echo "2. Resta 2\n";
        echo "3. Producto 3\n";
        echo "4. Escalar 4\n";
        echo "5. Salir\n";
    
        // Obtener la elección del usuario
        $opcion = readline("Seleccione una opción: ");
    
        // Procesar la elección del usuario
        switch ($opcion) {
            case 1:
                $resultado = operacionBasica($matriz, $matriz);
                echoMatriz($resultado);
                break;
            case 2:
                $resultado = operacionBasica($matriz, $matriz, "-");
                echoMatriz($resultado);
                break;
            case 3:
                $resultado = operacionBasica($matriz, $matriz, "*");
                echoMatriz($resultado);
                break;
            case 4:
                $matrizEscalar = escalar($matriz, 5);
                echoMatriz($matrizEscalar);
                break;
            case 5:
                exit;
            default:
                echo "Opción no válida. Por favor, seleccione una opción válida.\n";
                break;
        }
    }